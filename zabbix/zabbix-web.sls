pull-zabbix-web-image:
  docker_image.present:
    - name: "{{ pillar['zabbix']['zabbix_web_image']['name'] }}:{{ pillar['zabbix']['zabbix_web_image']['version'] }}"
    - force: True

generate-zabbix-web-configuration:
  file.managed:
    - name: /opt/containers/etc/nginx/conf.d/zabbix.conf
    - source: salt://zabbix/files/zabbix.conf.jinja
    - template: jinja
    - makedirs: True

running-zabbix-web-container:
  docker_container.running:
    - name: zabbix-web
    - image: "{{ pillar['zabbix']['zabbix_web_image']['name'] }}:{{ pillar['zabbix']['zabbix_web_image']['version'] }}"
    - force: True
    - restart_policy: always
    - binds:
      - "/opt/containers/etc/nginx/conf.d:/etc/nginx/conf.d"
    - port_bindings:
      - "8080:80"
      - "8443:443"
    - environment:
      - DB_SERVER_HOST: "{{ pillar['zabbix']['db']['db_server_host'] }}"
      - DB_SERVER_PORT: "{{ pillar['zabbix']['db']['db_server_port'] }}"
      - POSTGRES_USER: "{{ pillar['zabbix']['db']['postgres_user'] }}"
      - POSTGRES_PASSWORD: "{{ pillar['zabbix']['db']['postgres_password'] }}"
      - ZBX_SERVER_HOST: "{{ pillar['zabbix']['server']['zabbix_server_host'] }}"
      - PHP_TZ: "{{ pillar['zabbix']['zabbix_web_image']['php_tz'] }}"
    - require:
      - docker_image: pull-zabbix-web-image
