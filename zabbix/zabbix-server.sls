pull-zabbix-server-image:
  docker_image.present:
    - name: "{{ pillar['zabbix']['zabbix_server_image']['name'] }}:{{ pillar['zabbix']['zabbix_server_image']['version'] }}"
    - force: True

running-zabbix-server-container:
  docker_container.running:
    - name: zabbix-server
    - image: "{{ pillar['zabbix']['zabbix_server_image']['name'] }}:{{ pillar['zabbix']['zabbix_server_image']['version'] }}"
    - force: True
    - binds:
      - "/opt/containers/usr/lib/zabbix/alertscripts:/usr/lib/zabbix/alertscripts"
      - "/opt/containers/usr/lib/zabbix/externalscripts:/usr/lib/zabbix/externalscripts"
    - restart_policy: always
    - port_bindings:
      - "10050:10050"
      - "10051:10051"
    - environment:
      - DB_SERVER_HOST: "{{ pillar['zabbix']['db']['db_server_host'] }}"
      - DB_SERVER_PORT: "{{ pillar['zabbix']['db']['db_server_port'] }}"
      - POSTGRES_USER: "{{ pillar['zabbix']['db']['postgres_user'] }}"
      - POSTGRES_PASSWORD: "{{ pillar['zabbix']['db']['postgres_password'] }}"
      - POSTGRES_DB: "{{ pillar['zabbix']['db']['postgres_db'] }}"
    - require:
        - docker_image: pull-zabbix-server-image