{% if 'dev' in data.id %}
{% set env = 'dev' %}
{% elif 'prod' in data.id %}
{% set env = 'prod' %}
{% else %}
{% set env = 'none' %}
{% endif %}

set_environment:
  local.grains.setval:
    - tgt: {{ data['id'] }}
    - arg:
      - env
      - {{ env }}

log_new_minion:
  local.cmd.run:
    - name: log new minion
    - tgt: '*'
    - arg:
      - 'echo "[{{ data['id'] }}][minion started] A new Minion has (re)born on $(date)" >> /tmp/salt.reactor.log'
