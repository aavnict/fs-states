base:
  '*':
    - common.init
  '*zabbix*':
    - docker.init
#dev:
  'G@env:dev and *db*':
    - match: compound
    - postgresql.init
  'G@env:dev and *zabbix-server*':
    - match: compound
    - zabbix.zabbix-server
  'G@env:dev and *zabbix-web*':
    - match: compound
    - zabbix.zabbix-web
  'G@env:dev and *proxy*':
    - match: compound
    - nginx.init