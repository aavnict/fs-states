zabbix-repo:
 pkgrepo.managed:
    - humanname: Zabbix Official Repository - $basearch
    - baseurl: "http://repo.zabbix.com/zabbix/{{ pillar['common']['zabbix_agent']['version_repo'] }}/rhel/{{ grains['osmajorrelease']|int }}/$basearch/"
    - gpgcheck: 1
    - gpgkey: "{{ pillar['common']['zabbix_agent']['gpgkey'] }}"


zabbix-agent-install:
 pkg.installed:
    - pkgs:
      - zabbix-agent
    - require:
      - pkgrepo: zabbix-repo

zabbix-agent-conf:
 file.managed:
    - name: '/etc/zabbix/zabbix_agentd.d/agent.conf'
    - source: 'salt://common/files/zabbix_agent.conf.jinja'
    - template: jinja
    - mode: 0640
    - require:
      - pkg: zabbix-agent-install

zabbix-agent:
 service.running:
    - enable: True
    - require:
      - pkg: zabbix-agent-install
    - watch:
      - file: zabbix-agent-conf