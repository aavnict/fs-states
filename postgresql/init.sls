include:
  - postgresql.utils.swap_on
  - postgresql.utils.rewrite_pg_service
  - postgresql.utils.create_users
  - postgresql.utils.create_dirs
  - postgresql.utils.upstream
  - postgresql.server
  - postgresql.utils.add_libs
  - postgresql.utils.create_db_users
  - postgresql.utils.create_db
