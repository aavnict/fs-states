
{% if 1 == salt['cmd.retcode']('cat /etc/fstab | grep "/var/cache/swap/swap0"', python_shell=True) %}
# Define the swap size to be configured
activate-swap:
  cmd.run:
    - name: |
        mkdir -p /var/cache/swap
        dd if=/dev/zero of=/var/cache/swap/swap0 count={{ salt['pillar.get']('postgres:swap_size') }} bs=1M
        chmod 0600 /var/cache/swap/swap0
        mkswap /var/cache/swap/swap0
        echo /var/cache/swap/swap0    none    swap    sw      0 0 >> /etc/fstab
        swapon -a
{% endif %}
