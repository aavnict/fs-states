
{% from "postgresql/macros.jinja" import format_kwargs with context %}
postgresql-repo:
  pkgrepo.managed:
    {{- format_kwargs(salt['pillar.get']('postgres:pkg_repo')) }}
