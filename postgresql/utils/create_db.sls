{% from "postgresql/macros.jinja" import format_kwargs with context %}
{% if salt['pillar.get']('postgres:database') is defined %}
{% for db_name, args in salt['pillar.get']('postgres:database').iteritems() %}
create database {{ db_name }}:
  postgres_database.present:
    - name: {{ db_name }}
  {{- format_kwargs(args) }}
{% endfor %}
{% endif %}
