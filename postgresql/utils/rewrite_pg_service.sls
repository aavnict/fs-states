{% set version = salt['pillar.get']('postgres:version') %}
rewrite pg service:
  file.managed:
    - name: /etc/systemd/system/postgresql-{{ version }}.service
    - source: salt://postgresql/files/postgresql.service.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 744

systemd-reload:
  cmd.run:
    - name: systemctl daemon-reload
    - onchanges:
      - file: rewrite pg service
