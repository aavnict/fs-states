
{% set postgres_pkg = salt['pillar.get']('postgres:pkgs') %}
{% set pkgs_extra = salt['pillar.get']('postgres:pkgs_extra') %}

{% if salt['pillar.get']('postgres:pkgs_extra') %}
{% set pkgs = postgres_pkg + pkgs_extra %}
{% else %}
{% set pkgs = postgres_pkg %}
{% endif %}

install postgresql package:
  pkg.installed:
    - pkgs: {{ pkgs }}
    - refresh: True


{% if not salt['file.file_exists'](salt['pillar.get']('postgres:conf_dir') ~ '/PG_VERSION') %}
Initialize postgresql:
  cmd.run:
    - name: {{ salt['pillar.get']('postgres:prepare_cluster_command')  }}
    - runas: postgres
    - require:
      - pkg: install postgresql package
{% endif %}

generate pg_hba.conf:
  file.managed:
    - name: {{ salt['pillar.get']('postgres:conf_dir') }}/pg_hba.conf
    - source: salt://postgresql/files/pg_hba.conf.jinja
    - template: jinja
    - user: "postgres"
    - group: "postgres"
    - mode: 0640
    - watch_in:
      - service: reload postgresql

configure postgresql.conf:
  file.managed:
    - name: {{ salt['pillar.get']('postgres:conf_dir') }}/postgresql.conf
    - source: salt://postgresql/files/postgresql.conf.jinja
    - template: jinja
    - user: "postgres"
    - group: "postgres"
    - mode: 0640
    - watch_in:
      - service: reload postgresql

reload postgresql:
  service.running:
    - name: {{ salt['pillar.get']('postgres:service') }}
    - enable: True
    - reload: True
    - require:
      - pkg: install postgresql package
