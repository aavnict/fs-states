container-user:
  user.present:
    - fullname: 'Container User'
    - shell: /sbin/nologin
    - createhome: False
    - password: "{{ salt['cmd.run']('openssl passwd -1 ' ~ pillar['docker']['container_user']['password'] ) }}"

{% for sub_file in ['subuid', 'subgid'] %}
acitve_user_in_{{ sub_file }}:
  file.managed:
    - name: /etc/{{ sub_file }}
    - contents: container-user:10000:65536
    - require:
      - user: container-user
{% endfor %}

{% set checking_user_namespace = salt['cmd.retcode']('grubby --info="$(grubby --default-kernel)" | grep "user_namespace.enable=1"') %}

{% if checking_user_namespace == 1 %}
adding_user_namespae_argument:
  cmd.run:
    - name: grubby --args="user_namespace.enable=1" --update-kernel="$(grubby --default-kernel)"
{% endif %}

max_user_namespace_value:
  file.replace:
    - name: /proc/sys/user/max_user_namespaces
    - pattern: ^0
    - repl: "10"
    - backup: False

install_recommended_pkgs:
  pkg.installed:
    - pkgs:
      - epel-release
      - python2-pip
    - reload_modules: True

install_pip_recommended_modules:
  pip.installed:
    - pkgs:
      - docker
      - boto
    - require:
      - pkg: install_recommended_pkgs
