{% for user in pillar['docker']['docker_engine']['users'] %}
add_{{ user }}_to_docker_group:
  user.present:
    - name: {{ user }}
    - groups:
      - docker
    - remove_groups: False
{% endfor %}

