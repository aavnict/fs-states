clean_old_docker_engine:
{% if pillar['docker']['docker_engine']['new_install'] == True %}
 pkg.purged:
{% else %}
 pkg.removed:
{% endif %}
  - pkgs:
    - docker-ce
    - docker
    - docker-client
    - docker-client-latest
    - docker-common
    - docker-latest
    - docker-latest-logrotate
    - docker-logrotate
    - docker-selinux
    - docker-engine-selinux
    - docker-engine
    - container-selinux
    - docker-ce-cli

required_packages:
  pkg.installed:
    - pkgs:
      - python2-pip
      - yum-utils
      - device-mapper-persistent-data
      - lvm2

pip_pkgs_dependencies:
  pip.installed:
    - pkgs:
      - urllib3
      - pyOpenSSL
      - ndg-httpsclient
      - pyasn1
    - require:
      - pkg: required_packages

get_docker_ce_repo:
  file.managed:
    - source: https://download.docker.com/linux/centos/docker-ce.repo
    - name: '/etc/yum.repos.d/docker-ce.repo'
    - user: root
    - group: root
    - mode: 0644
    - skip_verify: True

docker_ce_edge_repo:
  ini.options_present:
    - name: /etc/yum.repos.d/docker-ce.repo
    - separator: '='
    - sections:
        docker-ce-edge:
          enabled: 0
        docker-ce-test:
          enabled: 0
    - require:
      - file: get_docker_ce_repo

docker_installing:
  pkg.installed:
    - name: docker-ce
{% if pillar['docker']['docker_engine']['version'] is defined %}
    - version: "*{{ pillar['docker']['docker_engine']['version'] }}*"
{% endif %}
    - refresh: True
    - allow_updates: True
    - require:
      - pip: pip_pkgs_dependencies
      - ini: docker_ce_edge_repo
      - pkg: required_packages

