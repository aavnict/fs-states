query_web:
  cmd.run:
    - name: curl -k https://127.0.0.1
#  http.query:
#    - name: 'http://127.0.0.1'
#    - status: 200
#   - method: GET
#   - verify_ssl: false

install policycoreutils-devel:
  pkg.installed:
    - name: policycoreutils-devel

audit2allow_cmd:
  cmd.run:
    - name: |
        grep nginx /var/log/audit/audit.log | audit2allow -M nginx;exit 0
    - cwd: /tmp/
    - require:
      - pkg: install policycoreutils-devel

semodule_cmd:
  cmd.run:
    - name: |
        semodule -i nginx.pp; exit 0
    - cwd: /tmp/
    - require:
      - cmd: audit2allow_cmd
