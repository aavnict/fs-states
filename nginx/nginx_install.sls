install centos epel:
  pkg.installed:
    - name: epel-release

install nginx packages:
  pkg.installed:
    - pkgs: {{ salt['pillar.get']('nginx:server:pkgs') }}
    - refresh: True
    - require:
      - pkg: install centos epel
